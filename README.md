# Portfolio

[API usage](https://gitlab.com/Drifterr/portfolio/-/tree/main/api)

[Database stuff in Python and PHP](https://gitlab.com/Drifterr/portfolio/-/tree/main/php%20and%20db)

[Blog site](https://gitlab.com/Drifterr/the-thing)