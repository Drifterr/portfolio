<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

$databaseFile = '/var/www/html/samu/database.db';
$db = new SQLite3($databaseFile);
if (!$db) {
    die("Error connecting to the database.");
}

$query1 = "SELECT *
FROM json_data
WHERE action LIKE 'A'
  AND timestamp >= DATETIME('now', 'start of day', 'localtime')
  AND timestamp < DATETIME('now', 'start of day', '+1 day', 'localtime')
ORDER BY timestamp ASC
LIMIT 1;
";

$result1 = $db->query($query1);
$latestData1 = $result1->fetchArray(SQLITE3_ASSOC);

$query2 = "SELECT *
FROM json_data
WHERE action LIKE 'B'
  AND timestamp >= DATETIME('now', 'start of day', 'localtime')
  AND timestamp < DATETIME('now', 'start of day', '+1 day', 'localtime')
ORDER BY timestamp DESC
LIMIT 1;";

$result2 = $db->query($query2);
$latestData2 = $result2->fetchArray(SQLITE3_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Samu Tracker</title>
    <link href="./style.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
</head>
<body>
    <h1>Samu Tracker</h1>
    <?php
    if ($latestData1) {
        echo "<p>Samu tuli kouluun viimeksi: {$latestData1['timestamp']}</p>";
    } else {
        echo "<p>Samu ei ole vielä tullut kouluun.</p>";
    }
    if ($latestData2) {
        echo "<p>Samu lähti koulusta viimeksi: {$latestData2['timestamp']}</p>";
    } else {
        echo "<p>Samu ei ole vielä lähtenyt koulusta.</p>";
    }

    if (preg_match('/\b(\d{2}:\d{2}):\d{2}\b/', $latestData1['timestamp'], $matches)) {
        $hourAndMinutes = $matches[1];

        // Check if the extracted time is larger than 08:00
        if ($hourAndMinutes > '08:00') {
            echo "<p id='b'>SAMU OLI MYÖHÄSSÄ!!!!</p>";
        }
    }
    if (preg_match('/\b(\d{2}:\d{2}):\d{2}\b/', $latestData2['timestamp'], $matches)) {
        $hourAndMinutes = $matches[1];

        // Check if the extracted time is smaller than 14:00
        if ($hourAndMinutes < '14:00') {
            echo "<p id='a'>SAMU LÄHTI AJOISSA!!!!</p>";
        }
    }
    ?>
<script src="./script.js"></script>
</body>
</html>