import json
import sqlite3
import time

# SQLite Database Connection
conn = sqlite3.connect('database.db')  # Replace with your database file
cursor = conn.cursor()

# Create a table if it doesn't exist
cursor.execute('''
    CREATE TABLE IF NOT EXISTS json_data (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        ref TEXT,
        action TEXT,
        timestamp DATE
    )
''')
conn.commit()

last_json_version = None

def load_and_compare_json():
    global last_json_version
    
    try:
        with open("/var/www/html/samu/data.json", "r") as file:
            data = json.load(file)

        if data["REF"] != last_json_version:
            last_json_version = data["REF"]

            # Insert data into SQLite database with timestamp
            timestamp = time.strftime('%Y-%m-%d %H:%M:%S')


            if data["REF"] == data["AA"]:
                handle_back_button_click()
                cursor.execute("INSERT INTO json_data (ref, action, timestamp) VALUES (?, ?, ?)", (data["REF"], "A", timestamp))
                conn.commit()
                print("a")
            elif data["REF"] == data["BB"]:
                handle_forward_button_click()
                cursor.execute("INSERT INTO json_data (ref, action, timestamp) VALUES (?, ?, ?)", (data["REF"], "B", timestamp))
                conn.commit()
                print("b")
    except FileNotFoundError:
        print("File not found.")
    except json.decoder.JSONDecodeError as e:
        print(f"Error decoding JSON: {e}")

def handle_back_button_click():
    # Implement the logic for handling back button click
    pass

def handle_forward_button_click():
    # Implement the logic for handling forward button click
    pass

def main():
    while True:
        load_and_compare_json()
        time.sleep(0.1)  # Adjust the sleep time according to your needs (10 milliseconds = 0.01 seconds)

if __name__ == "__main__":
    main()

# Close the database connection when done
conn.close()
